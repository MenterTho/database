CREATE TRIGGER after_insert_lophoc
AFTER INSERT ON public.lophoc
FOR EACH ROW
EXECUTE FUNCTION delete_loglophoc();

CREATE TRIGGER after_delete_lophoc
AFTER DELETE ON public.lophoc
FOR EACH ROW
EXECUTE FUNCTION insert_loglophoc();