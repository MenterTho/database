CREATE TRIGGER after_insert_sinhvien
AFTER INSERT ON public.sinhvien
FOR EACH ROW
EXECUTE FUNCTION delete_logsinhvien();

CREATE TRIGGER after_delete_sinhvien
AFTER DELETE ON public.sinhvien
FOR EACH ROW
EXECUTE FUNCTION insert_logsinhvien();