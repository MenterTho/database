CREATE TRIGGER after_insert_monhoc
AFTER INSERT ON public.monhoc
FOR EACH ROW
EXECUTE FUNCTION delete_logmonhoc();

CREATE TRIGGER after_delete_monhoc
AFTER DELETE ON public.monhoc
FOR EACH ROW
EXECUTE FUNCTION insert_logmonhoc();