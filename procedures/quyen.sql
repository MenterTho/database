/* Insert */
CREATE OR REPLACE PROCEDURE insert_quyen(
    p_name TEXT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.quyen
    SELECT COUNT(log.quyen.quyen_id) INTO log_count FROM log.quyen;

    -- Nếu tổng giá trị trong bảng log.quyen lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.quyen.public_quyen_id FROM log.quyen
                ORDER BY log.quyen.public_quyen_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO quyen (quyen_id, quyen_name)
            VALUES (item, p_name);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(quyen_id) OVER (ORDER BY quyen_id DESC)
            INTO value_last FROM quyen;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO quyen (quyen_id, quyen_name)
                VALUES (value_last+1, p_name);
        ELSE
            INSERT INTO quyen (quyen_id, quyen_name)
                VALUES (1, p_name);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_quyen(
    p_id   INT,
    p_name TEXT
) AS
$$
BEGIN
    UPDATE quyen
    SET quyen_name = p_name
    WHERE quyen_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_quyen(p_id INT)
AS
$$
BEGIN
    DELETE FROM quyen
    WHERE quyen_id = p_id;
END;
$$
LANGUAGE plpgsql;