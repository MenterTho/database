/* Thêm dữ liệu dangkylophoc */
CREATE OR REPLACE PROCEDURE insert_dangkylophoc(
    p_sinhvien_id INT,
    p_lophoc_id   INT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.dangkylophoc
    SELECT COUNT(log.dangkylophoc.dangkylophoc_id) INTO log_count
        FROM log.dangkylophoc;

    -- Nếu tổng giá trị trong bảng log.dangkylophoc lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.dangkylophoc.public_dangkylophoc_id
                FROM log.dangkylophoc
                ORDER BY log.dangkylophoc.public_dangkylophoc_id ASC
                LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO dangkylophoc (dangkylophoc_id, sinhvien_id, lophoc_id)
            VALUES (item, p_sinhvien_id, p_lophoc_id);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(dangkylophoc_id) OVER (ORDER BY dangkylophoc_id DESC)
            INTO value_last FROM dangkylophoc;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO dangkylophoc (dangkylophoc_id, sinhvien_id, lophoc_id)
                VALUES (value_last+1, p_sinhvien_id, p_lophoc_id);
        ELSE
            INSERT INTO dangkylophoc (dangkylophoc_id, sinhvien_id, lophoc_id)
                VALUES (1, p_sinhvien_id, p_lophoc_id);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Cập nhập dữ liệu dangkylophoc */
CREATE OR REPLACE PROCEDURE update_dangkylophoc(
    p_id          INT,
    p_sinhvien_id INT,
    p_lophoc_id   INT
) AS
$$
BEGIN
    UPDATE dangkylophoc
    SET
        sinhvien_id = p_sinhvien_id,
        lophoc_id = p_lophoc_id
    WHERE dangkylophoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu dangkylophoc */
CREATE OR REPLACE PROCEDURE delete_dangkylophoc(
    p_id INT
) AS
$$
BEGIN
    DELETE FROM dangkylophoc WHERE dangkylophoc_id = p_id;
END;
$$
LANGUAGE plpgsql;