/* Insert */
CREATE OR REPLACE PROCEDURE insert_cautraloi(
    p_cauhoi_id  INT,
    p_name       TEXT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
-- Lấy tổng giá trị trong bảng log.cautraloi
    SELECT COUNT(log.cautraloi.cautraloi_id) INTO log_count FROM log.cautraloi;

    -- Nếu tổng giá trị trong bảng log.cautraloi lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.cautraloi.public_cautraloi_id FROM log.cautraloi
                ORDER BY log.cautraloi.public_cautraloi_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO cautraloi (cautraloi_id, cauhoi_id, cautraloi_name)
            VALUES (item, p_cauhoi_id, p_name);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(cautraloi_id) OVER (ORDER BY cautraloi_id DESC)
            INTO value_last FROM cautraloi;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO cautraloi (cautraloi_id, cauhoi_id, cautraloi_name)
                VALUES (value_last+1, p_cauhoi_id, p_name);
        ELSE
            INSERT INTO cautraloi (cautraloi_id, cauhoi_id, cautraloi_name)
                VALUES (1, p_cauhoi_id, p_name);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_cautraloi(
    p_id         INT,
    p_cauhoi_id  INT,
    p_name       TEXT
) AS
$$
BEGIN
    UPDATE cautraloi
    SET
        cauhoi_id = p_cauhoi_id,
        cautraloi_name = p_name
    WHERE cautraloi_id = p_id;
END;
$$
LANGUAGE plpgsql;