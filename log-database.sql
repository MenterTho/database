CREATE SCHEMA log;

CREATE TABLE log.lophoc(
    lophoc_id         INT PRIMARY KEY,
    public_lophoc_id  INT UNIQUE
);

CREATE TABLE log.monhoc(
    monhoc_id        INT PRIMARY KEY,
    public_monhoc_id INT UNIQUE
);

CREATE TABLE log.dethimonhoc(
    dethimonhoc_id        INT PRIMARY KEY,
    public_dethimonhoc_id INT UNIQUE
);

CREATE TABLE log.thoigiandethichinh(
    thoigiandethichinh_id        INT PRIMARY KEY,
    public_thoigiandethichinh_id INT UNIQUE
);

CREATE TABLE log.chuong(
    chuong_id        INT PRIMARY KEY,
    public_chuong_id INT UNIQUE
);

CREATE TABLE log.cauhoi(
    cauhoi_id        INT PRIMARY KEY,
    public_cauhoi_id INT UNIQUE
);

CREATE TABLE log.cautraloi(
    cautraloi_id        INT PRIMARY KEY,
    public_cautraloi_id INT UNIQUE
);

CREATE TABLE log.sinhvien(
    sinhvien_id        INT PRIMARY KEY,
    public_sinhvien_id INT UNIQUE
);

CREATE TABLE log.dangkylophoc(
    dangkylophoc_id        INT PRIMARY KEY,
    public_dangkylophoc_id INT UNIQUE
);

CREATE TABLE log.quyen(
    quyen_id        INT PRIMARY KEY,
    public_quyen_id INT UNIQUE
);