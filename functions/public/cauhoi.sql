/* Get depend on id */
CREATE OR REPLACE FUNCTION get_cauhoi_withid(
    p_id INT
)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ch.cauhoi_id, ch.chuong_id, ch.cautraloi_id, ch.cauhoi_name
        FROM CauHoi ch
        WHERE ch.cauhoi_id = p_id
        ORDER BY ch.cauhoi_id;
END;
$$
LANGUAGE plpgsql;

/* Get all depend on chuong with limit and offset */
CREATE OR REPLACE FUNCTION getall_cauhoi_on_chuong(
    p_id     INT,
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  TEXT
) AS
$$
DECLARE
    cauhoi_count INT;
BEGIN
    SELECT COUNT(ch.cauhoi_id) INTO cauhoi_count
    FROM CauHoi ch
    WHERE ch.chuong_id = p_id;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(cauhoi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(cauhoi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(cauhoi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT ch.cauhoi_id, ch.chuong_id, ch.cautraloi_id, ch.cauhoi_name
        FROM CauHoi ch
        WHERE ch.chuong_id = p_id
        ORDER BY ch.cauhoi_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy bảng CauHoi phụ thuộc vào MonHoc */
CREATE OR REPLACE FUNCTION getall_cauhoi_on_monhoc(
    p_id     INT,
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  TEXT
) AS
$$
DECLARE
    cauhoi_count INT;
BEGIN
    SELECT COUNT(ch.cauhoi_id) INTO cauhoi_count 
    FROM CauHoi ch
    INNER JOIN chuong c ON c.chuong_id = ch.chuong_id
    INNER JOIN monhoc mh ON mh.monhoc_id = c.monhoc_id
    WHERE mh.monhoc_id = p_id;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(cauhoi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(cauhoi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(cauhoi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT ch.cauhoi_id, ch.chuong_id, ch.cautraloi_id, ch.cauhoi_name
        FROM CauHoi ch
        INNER JOIN chuong c ON c.chuong_id = ch.chuong_id
        INNER JOIN monhoc mh ON mh.monhoc_id = c.monhoc_id
        WHERE mh.monhoc_id = p_id
        ORDER BY ch.cauhoi_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy bảng CauHoi dựa vào MonHoc */
CREATE OR REPLACE FUNCTION getall_cauhoi_on_monhocid(
    p_id INT
)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ch.cauhoi_id, ch.chuong_id, ch.cautraloi_id, ch.cauhoi_name
        FROM CauHoi ch
        INNER JOIN Chuong c ON ch.chuong_id = c.chuong_id
        INNER JOIN MonHoc mh ON c.monhoc_id = mh.monhoc_id 
        WHERE mh.monhoc_id = p_id
        ORDER BY ch.cauhoi_id;
END;
$$
LANGUAGE plpgsql;

/* Get all with limit and offset */
CREATE OR REPLACE FUNCTION getall_cauhoi_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  TEXT
) AS
$$
DECLARE
    cauhoi_count INT;
BEGIN
    SELECT COUNT(ch.cauhoi_id) INTO cauhoi_count FROM cauhoi ch;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(cauhoi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(cauhoi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(cauhoi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT ch.cauhoi_id, ch.chuong_id, ch.cautraloi_id, ch.cauhoi_name
        FROM CauHoi ch
        ORDER BY ch.cauhoi_id
        LIMIT p_limit
        OFFSET p_offset * p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy giá trị cuối của bảng cauhoi */
CREATE OR REPLACE FUNCTION getlast_cauhoi()
RETURNS TABLE (
    cauhoi_last INT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT LAST_VALUE(cauhoi_id) OVER (ORDER BY cauhoi_id DESC)
        FROM CauHoi
        LIMIT 1;
END;
$$
LANGUAGE plpgsql;

/* Lấy tổng giá trị trong bảng cauhoi */
CREATE OR REPLACE FUNCTION getcount_cauhoi()
RETURNS INT AS
$$
DECLARE
    count INT;
BEGIN
    SELECT COUNT(cauhoi_id) INTO count
    FROM CauHoi;

    RETURN count;
END;
$$
LANGUAGE plpgsql;

/* Get all depend on search with limit and offset */
CREATE OR REPLACE FUNCTION searchall_cauhoi(
    p_keyword TEXT,
    p_limit   INT,
    p_offset  INT
)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ch.cauhoi_id, ch.chuong_id, ch.cautraloi_id, ch.cauhoi_name
        FROM CauHoi ch
        WHERE ch.cauhoi_name ILIKE '%' || p_keyword || '%'
        ORDER BY ch.cauhoi_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION prevent_if_cautraloi_not_in_cauhoi()
RETURNS TRIGGER AS
$$
DECLARE
    compare_id INT;
BEGIN
    SELECT cauhoi_id INTO compare_id FROM cauhoi WHERE cauhoi_id = NEW.cauhoi_id;

    IF (NOT EXISTS(SELECT 1 FROM cautraloi WHERE cautraloi_id = NEW.cautraloi_id AND cauhoi_id = compare_id)) THEN
        RAISE EXCEPTION 'Updating cauhoi with cautraloi not exist is not allowed';
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;
