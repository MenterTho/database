/* Lấy mỗi dữ liệu mật khẩu */
CREATE OR REPLACE FUNCTION get_nguoidung_withaccount(
    p_account TEXT
)
RETURNS TABLE (
    nguoidung_account     TEXT,
    quyen_id              INT,
    nguoidung_password    TEXT,
    nguoidung_displayname TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            nd.nguoidung_account,
            nd.quyen_id,
            nd.nguoidung_password,
            nd.nguoidung_displayname
        FROM NguoiDung nd
        WHERE nd.nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;

/* Lấy mỗi dữ liệu mật khẩu */
CREATE OR REPLACE FUNCTION get_nguoidung_withoutdisplayname(
    p_account TEXT
)
RETURNS TABLE (
    nguoidung_account     TEXT,
    quyen_id              INT,
    nguoidung_password    TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            nd.nguoidung_account,
            nd.quyen_id,
            nd.nguoidung_password
        FROM NguoiDung nd
        WHERE nd.nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;

/* Lấy mỗi dữ liệu mật khẩu */
CREATE OR REPLACE FUNCTION get_nguoidung_password(
    p_account TEXT
)
RETURNS TABLE (
    nguoidung_password TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT nd.nguoidung_password FROM NguoiDung nd
        WHERE nd.nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;

/* Lấy dữ liệu bảng nguoidung với limit và offset */
CREATE OR REPLACE FUNCTION getall_nguoidung_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    nguoidung_account     TEXT,
    quyen_id              INT,
    nguoidung_password    TEXT,
    nguoidung_displayname TEXT
) AS
$$
DECLARE
    nguoidung_count INT;
BEGIN
    SELECT COUNT(nd.nguoidung_account) INTO nguoidung_count FROM NguoiDung nd;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(nguoidung_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(nguoidung_count::NUMERIC / p_limit) > 0
                         THEN CEIL(nguoidung_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            nd.nguoidung_account,
            nd.quyen_id,
            nd.nguoidung_password,
            nd.nguoidung_displayname
        FROM NguoiDung nd
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;
