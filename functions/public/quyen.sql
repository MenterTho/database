/* Lấy bảng quyền chứa admin */
CREATE OR REPLACE FUNCTION get_quyen_haveadmin()
RETURNS TABLE (
    quyen_id   INT,
    quyen_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT q.quyen_id, q.quyen_name
        FROM Quyen q
        WHERE LOWER(q.quyen_name) = 'admin';
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_quyen_withlimitoffset(
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    quyen_id   INT,
    quyen_name TEXT
) AS
$$
DECLARE
    quyen_count INT;
BEGIN
    SELECT COUNT(q.quyen_id) INTO quyen_count FROM quyen q;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(quyen_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(quyen_count::NUMERIC / p_limit) > 0
                         THEN CEIL(quyen_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT q.quyen_id, q.quyen_name
        FROM Quyen q
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;
