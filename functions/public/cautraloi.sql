/* Get with id */
CREATE OR REPLACE FUNCTION get_cautraloi_withid(
    p_id INT
)
RETURNS TABLE (
    cautraloi_id   INT,
    cauhoi_id      INT,
    cautraloi_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ctl.cautraloi_id, ctl.cauhoi_id, ctl.cautraloi_name
        FROM CauTraLoi ctl
        WHERE ctl.cautraloi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Lấy các bảng phụ thuộc vào CauHoi */
CREATE OR REPLACE FUNCTION getall_cautraloi_on_cauhoi(
    p_id INT
)
RETURNS TABLE (
    cautraloi_id   INT,
    cauhoi_id      INT,
    cautraloi_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ctl.cautraloi_id, ctl.cauhoi_id, ctl.cautraloi_name
        FROM CauTraLoi ctl
        WHERE ctl.cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get all with limit and offset */
CREATE OR REPLACE FUNCTION getall_cautraloi_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    cautraloi_id   INT,
    cauhoi_id      INT,
    cautraloi_name TEXT
) AS
$$
DECLARE
    cautraloi_count INT;
BEGIN
    SELECT COUNT(ctl.cautraloi_id) INTO cautraloi_count FROM cautraloi ctl;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(cautraloi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(cautraloi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(cautraloi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT ctl.cautraloi_id, ctl.cauhoi_id, ctl.cautraloi_name
        FROM CauTraLoi ctl
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Giới hạn 4 giá trị CauTraloi phụ thuộc vào CauHoi */
CREATE OR REPLACE FUNCTION limit_cautraloi()
RETURNS TRIGGER AS $$
DECLARE
    cautraloi_count INT;
BEGIN
    -- Đếm số lượng giá trị cho cauhoi_id trong CauTraLoi
    SELECT COUNT(*) INTO cautraloi_count
        FROM CauTraLoi
        WHERE cauhoi_id = NEW.cauhoi_id;

    -- Kiểm tra nếu số lượng giá trị vượt quá 4, không chấp nhận chèn dữ liệu mới
    IF cautraloi_count >= 4 THEN
        RAISE EXCEPTION 'Exceeded maximum limit of 4 entries for cauhoi_id %', NEW.cauhoi_id;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

/* Lấy hết bảng dùng tìm kiếm keyword (cautraloi_name chứa gì) với limit và offset */
CREATE OR REPLACE FUNCTION searchall_cautraloi(
    p_keyword TEXT,
    p_limit   INT,
    p_offset  INT
)
RETURNS TABLE (
    cautraloi_id   INT,
    cauhoi_id      INT,
    cautraloi_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ctl.cautraloi_id, ctl.cauhoi_id, ctl.cautraloi_name
        FROM CauTraLoi ctl
        WHERE ctl.cautraloi_name ILIKE '%' || p_keyword || '%'
        ORDER BY ctl.cautraloi_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;