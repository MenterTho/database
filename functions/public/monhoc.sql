/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_monhoc_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    monhoc_id   INT,
    lophoc_id   INT,
    monhoc_name TEXT
) AS
$$
DECLARE
    monhoc_count INT;
BEGIN
    SELECT COUNT(mh.monhoc_id) INTO monhoc_count FROM MonHoc mh;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(monhoc_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(monhoc_count::NUMERIC / p_limit) > 0
                         THEN CEIL(monhoc_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT mh.monhoc_id, mh.lophoc_id, mh.monhoc_name
        FROM MonHoc mh
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy monhoc theo sinhvien */
CREATE OR REPLACE FUNCTION getall_monhoc_on_sinhvien(
    p_sinhvien_id INT
)
RETURNS TABLE (
    monhoc_id   INT,
    lophoc_id   INT,
    monhoc_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT mh.monhoc_id, mh.lophoc_id, mh.monhoc_name
        FROM monhoc mh
        INNER JOIN lophoc lh ON mh.lophoc_id = lh.lophoc_id
        INNER JOIN dangkylophoc dklh ON lh.lophoc_id = dklh.lophoc_id
        INNER JOIN sinhvien sv ON dklh.sinhvien_id = sv.sinhvien_id
        WHERE sv.sinhvien_id = p_sinhvien_id
        ORDER BY mh.monhoc_id;
END;
$$
LANGUAGE plpgsql;

/* Tìm kiếm theo monhoc_name */
CREATE OR REPLACE FUNCTION searchall_monhoc(
    p_keyword TEXT,
    p_limit   INT,
    p_offset  INT
)
RETURNS TABLE (
    monhoc_id   INT,
    lophoc_id   INT,
    monhoc_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT mh.monhoc_id, mh.lophoc_id, mh.monhoc_name
        FROM monhoc mh
        WHERE mh.monhoc_name ILIKE '%' || p_keyword || '%'
        ORDER BY mh.monhoc_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;