/* Lấy theo id bảng lophoc */
CREATE OR REPLACE FUNCTION get_lophoc_withid(
    p_id INT
)
RETURNS TABLE (
    lophoc_id   INT,
    lophoc_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT lh.lophoc_id, lh.lophoc_name
        FROM LopHoc lh
        WHERE lh.lophoc_id = p_id
        ORDER BY lh.lophoc_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_lophoc_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    lophoc_id   INT,
    lophoc_name TEXT
) AS
$$
DECLARE
    lophoc_count INT;
BEGIN
    SELECT COUNT(lh.lophoc_id) INTO lophoc_count FROM LopHoc lh;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(lophoc_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(lophoc_count::NUMERIC / p_limit) > 0
                         THEN CEIL(lophoc_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT lh.lophoc_id, lh.lophoc_name
        FROM LopHoc lh
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy số lượng monhoc co trong lophoc */
CREATE OR REPLACE FUNCTION getcount_lophoc_on_monhoc(
    p_id INT
)
RETURNS INT AS
$$
DECLARE
    monhoc_count INT;
BEGIN
    SELECT COUNT(mh.monhoc_id) INTO monhoc_count FROM monhoc mh
    INNER JOIN lophoc lh ON mh.lophoc_id = lh.lophoc_id
    WHERE lh.lophoc_id = p_id;

    RETURN monhoc_count;
END;
$$
LANGUAGE plpgsql;

/* Lấy số lượng sinhvien có trong lophoc */
CREATE OR REPLACE FUNCTION getcount_lophoc_on_sinhvien(
    p_id INT
)
RETURNS INT AS
$$
DECLARE
    sinhvien_count INT;
BEGIN
    SELECT COUNT(sv.sinhvien_id) INTO sinhvien_count
    FROM Sinhvien sv
    INNER JOIN dangkylophoc dklh ON sv.sinhvien_id = dklh.sinhvien_id
    INNER JOIN lophoc lh ON dklh.lophoc_id = lh.lophoc_id
    WHERE lh.lophoc_id = p_id;

    RETURN sinhvien_count;
END;
$$
LANGUAGE plpgsql;

/* Tìm kiếm theo lophoc_name */
CREATE OR REPLACE FUNCTION searchall_lophoc(
    p_keyword TEXT,
    p_limit   INT,
    p_offset  INT
)
RETURNS TABLE (
    lophoc_id   INT,
    lophoc_name TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT lh.lophoc_id, lh.lophoc_name
        FROM lophoc lh
        WHERE lh.lophoc_name ILIKE '%' || p_keyword || '%'
        ORDER BY lh.lophoc_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;