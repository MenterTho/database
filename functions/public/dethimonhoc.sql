/* Lấy bảng dethimonhoc dựa vào dethimonhoc_id */
CREATE OR REPLACE FUNCTION get_dethimonhoc_withid(
    p_id INT
)
RETURNS TABLE (
    dethimonhoc_id        INT,
    monhoc_id             INT,
    dethimonhoc_name      TEXT,
    dethimonhoc_real      BOOLEAN,
    dethimonhoc_questions INT,
    dethimonhoc_time      INT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            dtmh.dethimonhoc_id,
            dtmh.monhoc_id,
            dtmh.dethimonhoc_name,
            dtmh.dethimonhoc_real,
            dtmh.dethimonhoc_questions,
            dtmh.dethimonhoc_time
        FROM dethimonhoc dtmh
        WHERE dtmh.dethimonhoc_id = p_id
        ORDER BY dtmh.dethimonhoc_id;
END;
$$
LANGUAGE plpgsql;

/* Lấy các bảng DeThiMonHoc với limit và offset */
CREATE OR REPLACE FUNCTION getall_dethimonhoc_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    dethimonhoc_id        INT,
    monhoc_id             INT,
    dethimonhoc_name      TEXT,
    dethimonhoc_real      BOOLEAN,
    dethimonhoc_questions INT,
    dethimonhoc_time      INT
) AS
$$
DECLARE
    dethimonhoc_count INT;
BEGIN
    SELECT COUNT(dtmh.dethimonhoc_id) INTO dethimonhoc_count
    FROM DeThiMonHoc dtmh;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(dethimonhoc_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(dethimonhoc_count::NUMERIC / p_limit) > 0
                         THEN CEIL(dethimonhoc_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            dtmh.dethimonhoc_id,
            dtmh.monhoc_id,
            dtmh.dethimonhoc_name,
            dtmh.dethimonhoc_real,
            dtmh.dethimonhoc_questions,
            dtmh.dethimonhoc_time
        FROM DeThiMonHoc dtmh
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy các bảng phụ thuộc vào bảng MonHoc với limit và offset */
CREATE OR REPLACE FUNCTION getall_dethimonhoc_on_monhoc(
    p_id     INT,
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    dethimonhoc_id        INT,
    monhoc_id             INT,
    dethimonhoc_name      TEXT,
    dethimonhoc_real      BOOLEAN,
    dethimonhoc_questions INT,
    dethimonhoc_time      INT
) AS
$$
DECLARE
    dethimonhoc_count INT;
BEGIN
    SELECT COUNT(dtmh.dethimonhoc_id) INTO dethimonhoc_count
    FROM DeThiMonHoc dtmh WHERE dtmh.monhoc_id = p_id;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(dethimonhoc_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(dethimonhoc_count::NUMERIC / p_limit) > 0
                         THEN CEIL(dethimonhoc_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            dtmh.dethimonhoc_id,
            dtmh.monhoc_id,
            dtmh.dethimonhoc_name,
            dtmh.dethimonhoc_real,
            dtmh.dethimonhoc_questions,
            dtmh.dethimonhoc_time
        FROM DeThiMonHoc dtmh
        WHERE dtmh.monhoc_id = p_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy tổng giá trị trong bảng dethimonhoc */
CREATE OR REPLACE FUNCTION getcount_dethimonhoc()
RETURNS INT AS
$$
DECLARE
    dethimonhoc_count INT;
BEGIN
    SELECT COUNT(dtmh.dethimonhoc_id) INTO dethimonhoc_count
    FROM dethimonhoc dtmh;

    RETURN dethimonhoc_count;
END;
$$
LANGUAGE plpgsql;

/* Ngăn chặn việc cập nhập giá trị dethimonhoc_real
 * nếu thoigiandethichinh phụ thuộc */
CREATE OR REPLACE FUNCTION prevent_update_if_thoigiandethichinh_depend()
RETURNS TRIGGER AS $$
DECLARE
    value_real               BOOLEAN;
    thoigiandethichinh_count INT;
BEGIN
    -- Lấy giá trị dethimonhoc_real
    SELECT NEW.dethimonhoc_real INTO value_real;
    SELECT COUNT(tg.thoigiandethichinh_id) INTO thoigiandethichinh_count
    FROM thoigiandethichinh tg
    WHERE tg.dethimonhoc_id = NEW.dethimonhoc_id;

    -- So sánh real có false không
    IF (value_real = false AND thoigiandethichinh_count > 0) THEN
        -- Nếu có bất kỳ sự cố gắng INSERT nào, raise một exception
        RAISE EXCEPTION 'Updating dethimonhoc table is not allowed if thoigiandethichinh is depend on';
        RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện INSERT
    ELSE
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;