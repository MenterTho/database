/* Lấy các bảng DeThi phụ thuộc vào bảng MonHoc với limit và offset */
CREATE OR REPLACE FUNCTION getall_dethi_on_monhoc(
    p_id     INT,
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    dethi_id        INT,
    dethimonhoc_id  INT,
    sinhvien_id     INT,
    dethi_startdate TIMESTAMP WITH TIME ZONE,
    dethi_enddate   TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    dethi_count INT;
BEGIN
    SELECT COUNT(dt.dethi_id) INTO dethi_count FROM deThi dt
    INNER JOIN dethimonhoc dtmh ON dt.dethimonhoc_id = dtmh.dethimonhoc_id
    INNER JOIN monhoc mh ON dtmh.monhoc_id = mh.monhoc_id
    WHERE mh.monhoc_id = p_id;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(dethi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(dethi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(dethi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            dt.dethi_id,
            dt.dethimonhoc_id,
            dt.sinhvien_id,
            dt.dethi_startdate,
            dt.dethi_enddate
        FROM deThi dt
        INNER JOIN dethimonhoc dtmh ON dt.dethimonhoc_id = dtmh.dethimonhoc_id
        INNER JOIN monhoc mh ON dtmh.monhoc_id = mh.monhoc_id
        WHERE mh.monhoc_id = p_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy các bảng DeThi phụ thuộc vào bảng SinhVien với limit và offset */
CREATE OR REPLACE FUNCTION getall_dethi_on_sinhvien(
    p_id     INT,
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    dethi_id        INT,
    dethimonhoc_id  INT,
    sinhvien_id     INT,
    dethi_startdate TIMESTAMP WITH TIME ZONE,
    dethi_enddate   TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    dethi_count INT;
BEGIN
    SELECT COUNT(dt.dethi_id) INTO dethi_count FROM deThi dt
    WHERE sinhvien_id = p_id;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(dethi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(dethi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(dethi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            dt.dethi_id,
            dt.dethimonhoc_id,
            dt.sinhvien_id,
            dt.dethi_startdate,
            dt.dethi_enddate
        FROM deThi dt
        WHERE sinhvien_id = p_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy các bảng DeThi phụ thuộc thời gian */
CREATE OR REPLACE FUNCTION getall_dethi_on_time(
    p_time   TEXT,
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    dethi_id        INT,
    dethimonhoc_id  INT,
    sinhvien_id     INT,
    dethi_startdate TIMESTAMP WITH TIME ZONE,
    dethi_enddate   TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    dethi_count INT;
BEGIN
    SELECT COUNT(dt.dethi_id) INTO dethi_count FROM dethi dt
    WHERE dt.dethi_startdate::DATE = p_time::DATE OR
        dt.dethi_enddate::DATE = p_time::DATE;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(dethi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(dethi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(dethi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            dt.dethi_id,
            dt.dethimonhoc_id,
            dt.sinhvien_id,
            dt.dethi_startdate,
            dt.dethi_enddate
        FROM dethi dt
        WHERE dt.dethi_startdate::DATE = p_time::DATE OR
            dt.dethi_enddate::DATE = p_time::DATE
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy các bảng với limit và offset */
CREATE OR REPLACE FUNCTION getall_dethi_withlimitoffset(
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    dethi_id        INT,
    dethimonhoc_id  INT,
    sinhvien_id     INT,
    dethi_startdate TIMESTAMP WITH TIME ZONE,
    dethi_enddate   TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    dethi_count INT;
BEGIN
    SELECT COUNT(dt.dethi_id) INTO dethi_count FROM deThi dt;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(dethi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(dethi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(dethi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            dt.dethi_id,
            dt.dethimonhoc_id,
            dt.sinhvien_id,
            dt.dethi_startdate,
            dt.dethi_enddate
        FROM deThi dt
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Ngăn chặn việc thêm dethi nếu trước thời gian thi chính thức */
CREATE OR REPLACE FUNCTION prevent_insert_dethi_if_not_time_start()
RETURNS TRIGGER AS $$
DECLARE
    dethi_startdate     TIMESTAMP;
    value_current_time  TIMESTAMP;
BEGIN
    -- Lấy mốc thời gian đề thi bắt đầu và thời gian hiện tại
    SELECT tg.thoigiandethichinh_time INTO dethi_startdate FROM thoigiandethichinh tg
    INNER JOIN dethimonhoc dtmh ON tg.dethimonhoc_id = dtmh.dethimonhoc_id
    WHERE dtmh.dethimonhoc_id = NEW.dethimonhoc_id;
    SELECT NOW() AT TIME ZONE 'Asia/Ho_Chi_Minh' INTO value_current_time;

    -- So sánh 2 mốc thời gian
    IF (value_current_time < dethi_startdate) THEN
        -- Nếu có bất kỳ sự cố gắng INSERT nào, raise một exception
        RAISE EXCEPTION 'Inserting DeThi chinh thuc table is not allowed before start time';
        RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện INSERT
    ELSE
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;

/* Ngăn chặn việc sửa đề thi */
CREATE OR REPLACE FUNCTION prevent_update_dethi()
RETURNS TRIGGER AS $$
BEGIN
    -- Nếu có bất kỳ sự cố gắng UPDATE nào, raise một exception
    RAISE EXCEPTION 'Updating DeThi table is not allowed';
    RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện UPDATE
END;
$$ LANGUAGE plpgsql;

/* Ngăn chặn việc xóa đề thi */
CREATE OR REPLACE FUNCTION prevent_delete_dethi()
RETURNS TRIGGER AS $$
BEGIN
    -- Nếu có bất kỳ sự cố gắng DELETE nào, raise một exception
    RAISE EXCEPTION 'Deleting DeThi table is not allowed';
    RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện UPDATE
END;
$$ LANGUAGE plpgsql;