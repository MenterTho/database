/* Nhập dữ liệu cho bảng log.dethimonhoc */
CREATE OR REPLACE FUNCTION insert_logdethimonhoc()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.dethimonhoc
    SELECT LAST_VALUE(public.dethimonhoc.dethimonhoc_id)
        OVER (ORDER BY public.dethimonhoc.dethimonhoc_id DESC)
        INTO public_last FROM public.dethimonhoc;

    -- Lấy giá trị cuối của public.dethimonhoc
    SELECT LAST_VALUE(log.dethimonhoc.dethimonhoc_id) OVER
        (ORDER BY log.dethimonhoc.dethimonhoc_id DESC)
        INTO value_last FROM log.dethimonhoc;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.dethimonhoc_id < public_last) THEN
        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO log.dethimonhoc (dethimonhoc_id, public_dethimonhoc_id)
                VALUES (value_last+1, OLD.dethimonhoc_id);
        ELSE
            INSERT INTO log.dethimonhoc (dethimonhoc_id, public_dethimonhoc_id)
                VALUES (1, OLD.dethimonhoc_id);
        END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.dethimonhoc */
CREATE OR REPLACE FUNCTION delete_logdethimonhoc()
RETURNS TRIGGER AS
$$
DECLARE
    log_count                  INT;
    log_public_dethimonhoc_id INT;
BEGIN
    SELECT COUNT(log.dethimonhoc.dethimonhoc_id) INTO log_count FROM log.dethimonhoc;

    IF (log_count > 0) THEN
        SELECT public.dethimonhoc.dethimonhoc_id INTO log_public_dethimonhoc_id
            FROM public.dethimonhoc
            INNER JOIN log.dethimonhoc ON public.dethimonhoc.dethimonhoc_id = log.dethimonhoc.public_dethimonhoc_id
            ORDER BY public.dethimonhoc.dethimonhoc_id DESC LIMIT 1;

        DELETE FROM log.dethimonhoc WHERE log.dethimonhoc.public_dethimonhoc_id = log_public_dethimonhoc_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;