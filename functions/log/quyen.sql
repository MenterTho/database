/* Nhập dữ liệu cho bảng log.quyen */
CREATE OR REPLACE FUNCTION insert_logquyen()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.quyen
    SELECT LAST_VALUE(public.quyen.quyen_id)
        OVER (ORDER BY public.quyen.quyen_id DESC)
        INTO public_last FROM public.quyen;

    -- Lấy giá trị cuối của public.quyen
    SELECT LAST_VALUE(log.quyen.quyen_id) OVER
        (ORDER BY log.quyen.quyen_id DESC)
        INTO value_last FROM log.quyen;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.quyen_id < public_last) THEN
      -- Thêm dữ liệu
      IF (value_last >= 1) THEN
          INSERT INTO log.quyen (quyen_id, public_quyen_id)
              VALUES (value_last+1, OLD.quyen_id);
      ELSE
          INSERT INTO log.quyen (quyen_id, public_quyen_id)
              VALUES (1, OLD.quyen_id);
      END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.quyen */
CREATE OR REPLACE FUNCTION delete_logquyen()
RETURNS TRIGGER AS
$$
DECLARE
    log_count            INT;
    log_public_quyen_id INT;
BEGIN
    SELECT COUNT(log.quyen.quyen_id) INTO log_count FROM log.quyen;

    IF (log_count > 0) THEN
        SELECT public.quyen.quyen_id INTO log_public_quyen_id
            FROM public.quyen
            INNER JOIN log.quyen ON public.quyen.quyen_id = log.quyen.public_quyen_id
            ORDER BY public.quyen.quyen_id DESC LIMIT 1;

        DELETE FROM log.quyen WHERE log.quyen.public_quyen_id = log_public_quyen_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;