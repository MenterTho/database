/* Nhập dữ liệu cho bảng log.dangkylophoc */
CREATE OR REPLACE FUNCTION insert_logdangkylophoc()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.dangkylophoc
    SELECT LAST_VALUE(public.dangkylophoc.dangkylophoc_id)
        OVER (ORDER BY public.dangkylophoc.dangkylophoc_id DESC)
        INTO public_last FROM public.dangkylophoc;

    -- Lấy giá trị cuối của public.dangkylophoc
    SELECT LAST_VALUE(log.dangkylophoc.dangkylophoc_id) OVER
        (ORDER BY log.dangkylophoc.dangkylophoc_id DESC)
        INTO value_last FROM log.dangkylophoc;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.dangkylophoc_id < public_last) THEN
        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO log.dangkylophoc (dangkylophoc_id, public_dangkylophoc_id)
                VALUES (value_last+1, OLD.dangkylophoc_id);
        ELSE
            INSERT INTO log.dangkylophoc (dangkylophoc_id, public_dangkylophoc_id)
                VALUES (1, OLD.dangkylophoc_id);
        END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.dangkylophoc */
CREATE OR REPLACE FUNCTION delete_logdangkylophoc()
RETURNS TRIGGER AS
$$
DECLARE
    log_count                  INT;
    log_public_dangkylophoc_id INT;
BEGIN
    SELECT COUNT(log.dangkylophoc.dangkylophoc_id) INTO log_count FROM log.dangkylophoc;

    IF (log_count > 0) THEN
        SELECT public.dangkylophoc.dangkylophoc_id INTO log_public_dangkylophoc_id
            FROM public.dangkylophoc
            INNER JOIN log.dangkylophoc ON public.dangkylophoc.dangkylophoc_id = log.dangkylophoc.public_dangkylophoc_id
            ORDER BY public.dangkylophoc.dangkylophoc_id DESC LIMIT 1;

        DELETE FROM log.dangkylophoc WHERE log.dangkylophoc.public_dangkylophoc_id = log_public_dangkylophoc_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;