/* Nhập dữ liệu cho bảng log.cauhoi */
CREATE OR REPLACE FUNCTION insert_logcauhoi()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.cauhoi
    SELECT LAST_VALUE(public.cauhoi.cauhoi_id)
        OVER (ORDER BY public.cauhoi.cauhoi_id DESC)
        INTO public_last FROM public.cauhoi;

    -- Lấy giá trị cuối của public.cauhoi
    SELECT LAST_VALUE(log.cauhoi.cauhoi_id) OVER
        (ORDER BY log.cauhoi.cauhoi_id DESC)
        INTO value_last FROM log.cauhoi;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.cauhoi_id < public_last) THEN
        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO log.cauhoi (cauhoi_id, public_cauhoi_id)
                VALUES (value_last+1, OLD.cauhoi_id);
        ELSE
            INSERT INTO log.cauhoi (cauhoi_id, public_cauhoi_id)
                VALUES (1, OLD.cauhoi_id);
        END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.cauhoi */
CREATE OR REPLACE FUNCTION delete_logcauhoi()
RETURNS TRIGGER AS
$$
DECLARE
    log_count            INT;
    log_public_cauhoi_id INT;
BEGIN
    SELECT COUNT(log.cauhoi.cauhoi_id) INTO log_count FROM log.cauhoi;

    IF (log_count > 0) THEN
        SELECT public.cauhoi.cauhoi_id INTO log_public_cauhoi_id
            FROM public.cauhoi
            INNER JOIN log.cauhoi ON public.cauhoi.cauhoi_id = log.cauhoi.public_cauhoi_id
            ORDER BY public.cauhoi.cauhoi_id DESC LIMIT 1;

        DELETE FROM log.cauhoi WHERE log.cauhoi.public_cauhoi_id = log_public_cauhoi_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;