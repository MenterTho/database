/* Nhập dữ liệu cho logcautraloi */
CREATE OR REPLACE FUNCTION insert_logcautraloi()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.cautraloi
    SELECT LAST_VALUE(public.cautraloi.cautraloi_id)
        OVER (ORDER BY public.cautraloi.cautraloi_id DESC)
        INTO public_last FROM public.cautraloi;

    -- Lấy giá trị cuối của log.cautraloi
    SELECT LAST_VALUE(log.cautraloi.cautraloi_id) OVER
        (ORDER BY log.cautraloi.cautraloi_id DESC)
        INTO value_last FROM log.cautraloi;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.cautraloi_id < public_last) THEN
      -- Thêm dữ liệu
      IF (value_last >= 1) THEN
          INSERT INTO log.cautraloi (cautraloi_id, public_cautraloi_id)
              VALUES (value_last+1, OLD.cautraloi_id);
      ELSE
          INSERT INTO log.cautraloi (cautraloi_id, public_cautraloi_id)
              VALUES (1, OLD.cautraloi_id);
      END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION delete_logcautraloi()
RETURNS TRIGGER AS
$$
DECLARE
    log_count               INT;
    log_public_cautraloi_id INT;
BEGIN
    SELECT COUNT(log.cautraloi.cautraloi_id) INTO log_count FROM log.cautraloi;

    IF (log_count > 0) THEN
        SELECT public.cautraloi.cautraloi_id INTO log_public_cautraloi_id
            FROM public.cautraloi
            INNER JOIN log.cautraloi
                ON public.cautraloi.cautraloi_id = log.cautraloi.public_cautraloi_id
            ORDER BY public.cautraloi.cautraloi_id DESC LIMIT 1;

        DELETE FROM log.cautraloi
        WHERE log.cautraloi.public_cautraloi_id = log_public_cautraloi_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;