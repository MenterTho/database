/* Nhập dữ liệu cho bảng log.chuong */
CREATE OR REPLACE FUNCTION insert_logchuong()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.chuong
    SELECT LAST_VALUE(public.chuong.chuong_id)
        OVER (ORDER BY public.chuong.chuong_id DESC)
        INTO public_last FROM public.chuong;

    -- Lấy giá trị cuối của public.chuong
    SELECT LAST_VALUE(log.chuong.chuong_id) OVER
        (ORDER BY log.chuong.chuong_id DESC)
        INTO value_last FROM log.chuong;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.chuong_id < public_last) THEN
      -- Thêm dữ liệu
      IF (value_last >= 1) THEN
          INSERT INTO log.chuong (chuong_id, public_chuong_id)
              VALUES (value_last+1, OLD.chuong_id);
      ELSE
          INSERT INTO log.chuong (chuong_id, public_chuong_id)
              VALUES (1, OLD.chuong_id);
      END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.chuong */
CREATE OR REPLACE FUNCTION delete_logchuong()
RETURNS TRIGGER AS
$$
DECLARE
    log_count            INT;
    log_public_chuong_id INT;
BEGIN
    SELECT COUNT(log.chuong.chuong_id) INTO log_count FROM log.chuong;

    IF (log_count > 0) THEN
        SELECT public.chuong.chuong_id INTO log_public_chuong_id
            FROM public.chuong
            INNER JOIN log.chuong ON public.chuong.chuong_id = log.chuong.public_chuong_id
            ORDER BY public.chuong.chuong_id DESC LIMIT 1;

        DELETE FROM log.chuong WHERE log.chuong.public_chuong_id = log_public_chuong_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;